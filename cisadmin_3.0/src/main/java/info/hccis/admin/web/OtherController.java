package info.hccis.admin.web;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.util.Utility;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr){ 
        this.ctr = ctr;
        this.cvr = cvr;
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {
        System.out.println("in controller for /");
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        System.out.println("MD5 hash for 123="+Utility.getHashPassword("123"));
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);
        session.setAttribute("db", databaseConnection);
        
        //Setup the user types.  this will be for the default connection and 
        //can be changed if the user provides alternate db setup.
        ArrayList<CodeValue> UserTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "1");
        session.setAttribute("UserTypes", UserTypes);


        return "other/databaseInformation";
    }

}
